/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package api;

import calendariotaller.CalendarSample;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Events;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import entidades.Cuenta;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martín
 */
public class GoogleCalendar extends Thread {

    public GoogleCalendar() {
    }

    /**
     * Be sure to specify the name of your application. If the application name
     * is {@code null} or blank, the application will log a warning. Suggested
     * format is "MyCompany-ProductName/1.0".
     */
    private static final String APPLICATION_NAME = "Kalendaro";

    private String token;
    private String email;
    private String nombre;
    private String usuario;
   

    /**
     * Directory to store user credentials.
     */
    private static  java.io.File DATA_STORE_DIR;
            //new java.io.File("\\.store\\kalendaro");

    /**
     * Global instance of the {@link DataStoreFactory}. The best practice is to
     * make it a single globally shared instance across your application.
     */
    private static FileDataStoreFactory dataStoreFactory;

    /**
     * Global instance of the HTTP transport.
     */
    private static HttpTransport httpTransport;

    /**
     * Global instance of the JSON factory.
     */
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static com.google.api.services.calendar.Calendar client;

    /**
     * Authorizes the installed application to access user's protected data.
     */
    private static Credential authorize() throws IOException {
        // load client secrets
        Reader lector = new InputStreamReader(CalendarSample.class.getResourceAsStream("/calendariotaller/client_secrets.json"));
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, lector);

        if (clientSecrets.getDetails().getClientId().startsWith("Enter")
                || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
            System.out.println(
                    "Enter Client ID and Secret from https://code.google.com/apis/console/?api=calendar "
                    + "into calendar-cmdline-sample/src/main/resources/client_secrets.json");
            System.exit(1);
        }
        // set up authorization code flow
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                httpTransport, JSON_FACTORY, clientSecrets,
                Collections.singleton(CalendarScopes.CALENDAR + " https://www.googleapis.com/auth/userinfo.email"
                        + " https://www.googleapis.com/auth/userinfo.profile")).setDataStoreFactory(dataStoreFactory)
                .build();
        // authorize
        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");

    }

    public void iniciarSesion(String vUsuario) throws GeneralSecurityException, IOException, Exception {
        
        usuario= vUsuario;
        
        // initialize the transport
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();

        // initialize the data store factory
        DATA_STORE_DIR=new java.io.File(System.getProperty("user.home"), "Documents\\kalendaro\\"+usuario);
        dataStoreFactory = new FileDataStoreFactory(DATA_STORE_DIR);

        // authorization
        Credential credential = GoogleCalendar.authorize();
        token = credential.getAccessToken();

        URL url;
        InputStream is = null;
        BufferedReader br;
        String line;
        
        
        try {
            url = new URL("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + token);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));
            line = br.toString();
            
           int contador=0;
            while ((line = br.readLine()) != null) {
                if(contador==2){
                    email=line.substring(11, line.length()-2);
                }
                if(contador==4){
                    nombre=line.substring(10,line.length()-2);
                }
                contador++;
        }

        } catch (MalformedURLException mue) {
            mue.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
         
        // set up global Calendar instance
        client = new com.google.api.services.calendar.Calendar.Builder(
                httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();

    }

    @Override
    public void run() {
        try {
            this.iniciarSesion(usuario);
        } catch (IOException ex) {
            Logger.getLogger(GoogleCalendar.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(GoogleCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Cuenta dato() {
        Cuenta cuenta = new Cuenta();
        cuenta.setEmail(email);
        cuenta.setDisplayname(nombre);
        cuenta.setServicio("GoogleCalendar");
        cuenta.setToken(token);
        return cuenta;
    }

    public Events getEventos(String idCalendario) throws IOException, Exception {
        this.iniciarSesion(usuario);
        Events resultado= client.events().list(idCalendario)//.setTimeMin(new DateTime(System.currentTimeMillis()-180*86400000))
                .execute();
        return resultado;
        //return client.events().list(idCalendario).execute();
    }

    public Event nuevoEvento(String nombre, Date fechainicio, Date fechfin, String idCalendario) throws IOException {
        Event event = new Event();
        event.setSummary(nombre);
        Date endDate = fechfin;
        DateTime start = new DateTime(fechainicio, TimeZone.getTimeZone("UTC"));
        event.setStart(new EventDateTime().setDateTime(start));
        DateTime end = new DateTime(endDate, TimeZone.getTimeZone("UTC"));
        event.setEnd(new EventDateTime().setDateTime(end));
        event.setCreated(new DateTime(new Date()));
        return client.events().insert(idCalendario, event).execute();

    }
    
    public Event nuevoEvento(Event event, String idCalendario) throws IOException{
        Event result = client.events().insert(idCalendario, event).execute();
        return result;
    }

    public Calendar agregarCalendario(Calendar entry) throws IOException {
        Calendar result = client.calendars().insert(entry).execute();
        return result;
    }

    public List<CalendarListEntry> getCalendarios() throws IOException, Exception {
        /*Credential credential = GoogleCalendar.authorize();
        client = new com.google.api.services.calendar.Calendar.Builder(
                httpTransport, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
          */   
        
        List<CalendarListEntry> items;
        
        
        // Iterate through entries in calendar list
        String pageToken = null;
        do {
            CalendarList calendarList = client.calendarList().list().setPageToken(pageToken).execute();
            items = calendarList.getItems();
            pageToken = calendarList.getNextPageToken();
        } while (pageToken != null);
        return items;

    }

    public void eliminarCalendario(String calendar) throws IOException {
        client.calendars().delete(calendar).execute();
    }

    public void eliminarEvento(String event) throws IOException {
        client.events().delete("primary", event).execute();
    }

    public Calendar modificarCalendario(Calendar content) throws IOException {
        return client.calendars().patch(content.getId(), content).execute();
    }

    public Event modificarEvento(Event content, Calendar calendario) throws IOException {
        return client.events().patch(calendario.getId(), content.getId(), content).execute();
    }
}
