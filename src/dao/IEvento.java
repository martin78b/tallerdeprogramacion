/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Calendario;
import entidades.Evento;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;

/**
 *
 * @author Martin
 */
public interface IEvento {
    
    public void guardar(Evento evento) throws SQLIntegrityConstraintViolationException, SQLException;
    
    public void actualizar(Evento evento) throws SQLException;
    
    public void borrar (String eventoId);
    
    public Collection<Evento> cargar(Calendario calendario) throws SQLException;
    
    public Collection<Evento> cargarpornombre(String nombre, Calendario calendario) throws SQLException ;
}
