/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Calendario;
import entidades.Cuenta;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.spi.PersistenceProvider;
import org.hibernate.jpa.HibernatePersistenceProvider;

/**
 *
 * @author Mauricio
 */
public class CalendarioDao implements ICalendario {

    /**
     * Guarda un Calendario en la base de datos
     *
     * @param calendario
     */
    @Override
    public void guardar(Calendario calendario) {
           try {
            String dbURL = "jdbc:derby://localhost:1527/calendario";
            Connection conn = DriverManager.getConnection(dbURL);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("set schema APP");
            stmt.executeUpdate("INSERT INTO CALENDARIO (id,summary, description, location, "
                    + "tomezone, kind, cuenta) VALUES ('" + calendario.getId() + "',"
                    + "'" + calendario.getSummary() + "','" + calendario.getDescription() + "', "
                    + "'" + calendario.getLocation() + "', '" + calendario.getTomezone() + "' ,'" + calendario.getKind() + "', "
                    + "'" + calendario.getCuenta().getId() + "')");
        } catch (SQLException ex) {
            Logger.getLogger(CalendarioDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        /*
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        //PersistenceProvider provider = new HibernatePersistenceProvider();
        //EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(calendario);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            System.err.println(e);
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
        */
    }

    /**
     * Actualiza un calendario en la base de datos
     *
     * @param calendario
     */
    @Override
    public void actualizar(Calendario calendario) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Calendario calendarioDb = (Calendario) entityManager.find(Calendario.class, calendario.getId());
            calendarioDb.setSummary(calendario.getSummary());
            calendarioDb.setDescription(calendario.getDescription());
            calendarioDb.setKind(calendario.getKind());
            calendarioDb.setLocation(calendario.getLocation());
            calendarioDb.setTomezone(calendario.getTomezone());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    /**
     * Borra un calendario de la base de datos
     *
     */
    @Override
    public void borrar(Calendario calendario) {
        
        
            //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");
            
            
            PersistenceProvider provider = new HibernatePersistenceProvider();
            EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            /*
            entityManager.getTransaction().begin();
            Query query = entityManager.createNativeQuery("DELETE FROM CALENDARIO WHERE CALENDARIO.ID =  \"" + calendario.getId()+"\"");
            query.executeUpdate();
            */
            try {
            entityManager.getTransaction().begin();
            
            Calendario cal = (Calendario) entityManager.find(Calendario.class, calendario.getId());
            System.out.println(cal.getSummary());
            System.out.println(cal.getTomezone());
            entityManager.remove(entityManager.merge(cal));
            entityManager.getTransaction().commit();
            } catch (Exception e) {
            entityManager.getTransaction().rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
             
    }

    /**
     * Carga los calendarios asociados a una cuenta
     *
     * @param cuenta
     * @return la lista de calendarios de la cuenta especificada
     */
    @Override
    public Collection<Calendario> cargar(Cuenta cuenta) {
        Collection<Calendario> lista;
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        lista = entityManager.find(Cuenta.class, cuenta.getId()).getCalendarioCollection();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
        Set<Calendario> conjunto = new HashSet<>();
        conjunto.addAll(lista);
        lista.clear();
        lista.addAll(conjunto);
        return lista;
    }

}
