/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Usuario;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import org.hibernate.jpa.HibernatePersistenceProvider;

/**
 *
 * @author Martin
 */
public class UsuarioDAO implements IUsuario {

    @Override
    public void guardar(Usuario usuario) throws SQLIntegrityConstraintViolationException {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(usuario);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    @Override
    public void actualizar(Usuario usuario) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Usuario usuarioDb = (Usuario) entityManager.find(Usuario.class, usuario.getNombre());
            usuarioDb.setPassword(usuario.getPassword());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    @Override
    public void borrar(Usuario usuario) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(entityManager.merge(usuario));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    @Override
    public Usuario cargar(String nombre) throws SQLException, NullPointerException {
        Usuario user = null;
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            user = (Usuario)entityManager.find(Usuario.class, nombre);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
        return user;
    }
}
