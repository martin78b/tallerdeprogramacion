/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Cuenta;
import entidades.Usuario;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import org.hibernate.jpa.HibernatePersistenceProvider;

/**
 *
 * @author Martin
 */
public class CuentaDAO implements ICuenta {

    @Override
    public void guardar(String email, String displayname, String servicio, String token, String nombreUsuario) {
        Cuenta cuentaDb = new Cuenta();
        cuentaDb.setEmail(email);
        cuentaDb.setServicio(servicio);
        cuentaDb.setToken(token);

        try {
            String dbURL = "jdbc:derby://localhost:1527/calendario";
            Connection conn = DriverManager.getConnection(dbURL);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("set schema APP");
            ResultSet rs = stmt.executeQuery("SELECT max(ID) FROM CUENTA");
            rs.next();
            String resultado = rs.getString(1);
            System.out.println(resultado);
            int identificador = 0;
            try {
                identificador = Integer.parseInt(resultado);
            } catch (NumberFormatException ex) {
                identificador = 0;
            }
            identificador = identificador + 1;
            stmt.executeUpdate("INSERT INTO CUENTA (id, email, displayname, servicio, token, usuario) values ('" + identificador + "','" + email + "', '" + displayname + "', '" + servicio + "', '" + token + "', '" + nombreUsuario + "')");
        } catch (SQLException ex) {
            Logger.getLogger(CuentaDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void modificar(Cuenta cuenta) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Cuenta cuentaDb = (Cuenta) entityManager.find(Cuenta.class, cuenta.getId());
            cuentaDb.setDisplayname(cuenta.getDisplayname());
            cuentaDb.setEmail(cuenta.getEmail());
            cuentaDb.setServicio(cuenta.getServicio());
            cuentaDb.setToken(cuenta.getToken());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
    }

    @Override
    public void borrar(Cuenta cuenta) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        entityManager.remove(entityManager.merge(cuenta));
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();

    }

    @Override
    public Collection<Cuenta> cargar(Usuario user) {
        Collection<Cuenta> lista;
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        lista = entityManager.find(Usuario.class, user.getNombre()).getCuentaCollection();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
        Set<Cuenta> conjunto = new HashSet<>();
        conjunto.addAll(lista);
        lista.clear();
        lista.addAll(conjunto);
        return lista;
    }

    public Collection<Cuenta> cargar(String user) {
        Collection<Cuenta> lista = null;
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");

        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        lista = entityManager.find(Usuario.class, user).getCuentaCollection();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
        Set<Cuenta> conjunto = new HashSet<>();
        conjunto.addAll(lista);
        lista.clear();
        lista.addAll(conjunto);
        return lista;
    }

}
