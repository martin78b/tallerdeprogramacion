/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entidades.Calendario;
import entidades.Evento;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.spi.PersistenceProvider;
import org.hibernate.jpa.HibernatePersistenceProvider;

/**
 *
 * @author Martin
 */
public class EventoDAO implements IEvento {

    /**
     * Guarda un evento en la base de datos
     * @param evento
     * @throws SQLIntegrityConstraintViolationException
     * @throws SQLException 
     */
    @Override
    public void guardar(Evento evento) throws SQLIntegrityConstraintViolationException, SQLException {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");
        
        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            entityManager.persist(evento);
            entityManager.getTransaction().commit();
        } catch (Exception ex) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    /**
     * Actualiza un evento ya existente en la base de datos
     * @param evento
     * @throws SQLException 
     */
    @Override
    public void actualizar(Evento evento) throws SQLException {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");
        
        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Evento eventodb = (Evento) entityManager.find(Evento.class, evento.getId());
            eventodb.setStartdate(evento.getStartdate());
            eventodb.setSummary(evento.getSummary());
            eventodb.setStatus(evento.getStatus());
            eventodb.setUpdated(new Date());
            eventodb.setEnddate(evento.getEnddate());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    /**
     * Borra un evento de la base de datos
     * @param eventoId 
     */
    @Override
    public void borrar(String eventoId) {
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");
        
        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        try {
            entityManager.getTransaction().begin();
            Evento evento = (Evento) entityManager.find(Evento.class, eventoId);
            entityManager.remove(entityManager.merge(evento));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

    /**
     * Carga todos los eventos pertenecientes a un calendario
     * @param calendario
     * @return Collection<Evento>
     * @throws SQLException 
     */
    @Override
    public Collection<Evento> cargar(Calendario calendario) throws SQLException {
        Collection<Evento> lista;
        
        //EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("CalendarioTallerPU");
        PersistenceProvider provider = new HibernatePersistenceProvider();
        EntityManagerFactory entityManagerFactory = provider.createEntityManagerFactory("CalendarioTallerPU", null);
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        lista = entityManager.find(Calendario.class, calendario.getId()).getEventoCollection();
        entityManager.getTransaction().commit();
        entityManager.close();
        entityManagerFactory.close();
        Set<Evento> conjunto = new HashSet<>();
        conjunto.addAll(lista);
        lista.clear();
        lista.addAll(conjunto);
        return lista;
    }
    
    /**
     * Carga todos los eventos de una fecha de un calendario
     * @param idCalendario
     * @param fecha
     * @return La lista de los eventos de dicha fecha
     */
    public Collection<Evento> cargaPorFecha(String idCalendario, Date fecha){
        Collection<Evento> lista = new ArrayList<>();
        Evento evento = null;
        try {
            String dbURL = "jdbc:derby://localhost:1527/calendario";
            Connection conn = DriverManager.getConnection(dbURL);
            Statement stmt = conn.createStatement();
            stmt.executeUpdate("set schema APP");
            String st = "SELECT * FROM EVENTO WHERE EVENTO.STARTDATE='" + new java.sql.Date(fecha.getTime()) + "' AND EVENTO.CALENDARIO='" + idCalendario + "'";
            ResultSet rs = stmt.executeQuery(st);
            while (rs.next()) {
                
                evento = new Evento();
                evento.setId(rs.getString("ID"));
                evento.setStatus(rs.getString("STATUS"));
                evento.setCreated(rs.getDate("CREATED"));
                evento.setUpdated(rs.getDate("UPDATED"));
                evento.setSummary(rs.getString("SUMMARY"));
                evento.setDescription(rs.getString("DESCRIPTION"));
                evento.setLocation(rs.getString("LOCATION"));
                evento.setColorid(rs.getString("COLORID"));
                evento.setStartdate(rs.getDate("STARTDATE"));
                evento.setStartdatetime(rs.getTime("STARTDATETIME"));
                evento.setStarttimezone(rs.getString("STARTTIMEZONE"));
                evento.setEnddate(rs.getDate("ENDDATE"));
                evento.setEnddatetime(rs.getTime("ENDDATETIME"));
                evento.setEndtimezone(rs.getString("ENDTIMEZONE"));
                evento.setRecurrence(rs.getString("RECURRENCE"));
                evento.setRecurrenceeventid(rs.getString("RECURRENCEEVENTID"));
                evento.setOriginalstartdate(rs.getDate("ORIGINALSTARTDATE"));
                evento.setOriginalstartdatetime(rs.getTime("ORIGINALSTARTDATETIME"));
                evento.setOriginaltimezone(rs.getString("ORIGINALTIMEZONE"));
                evento.setTransparency(rs.getString("TRANSPARENCY"));
                evento.setVisibility(rs.getString("VISIBILITY"));
                evento.setIcaluid(rs.getString("ICALUID"));
                evento.setSequencia(rs.getString("SEQUENCIA"));
                evento.setRemindersdefault(rs.getBoolean("REMINDERSDEFAULT"));
                evento.setRemindermethod(rs.getString("REMINDERMETHOD"));
                evento.setReminderminutes(rs.getInt("REMINDERMINUTES"));
                lista.add(evento);
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        return lista;
    }

    /**
     * Carga en una lista los eventos de un calendario que tengan tal nombre
     * @param nombre
     * @param calendario
     * @return
     * @throws SQLException 
     */
    @Override
    public Collection<Evento> cargarpornombre(String nombre, Calendario calendario) throws SQLException {
        Collection<Evento> lista;
        lista = this.cargar(calendario);
        Set<Evento> conjunto = new HashSet<>();
        for (Evento next : lista) {
            if (next.getSummary().equalsIgnoreCase(nombre)) {
                conjunto.add(next);
            }
        }
        lista.clear();
        lista.addAll(conjunto);
        return lista;
    }
}
