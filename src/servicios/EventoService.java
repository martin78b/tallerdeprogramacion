/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import api.GoogleCalendar;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import dao.EventoDAO;
import entidades.Calendario;
import entidades.Evento;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mauricio
 */
public class EventoService implements IEventoServise {

    EventoDAO eventoDao = new EventoDAO();
    GoogleCalendar googleapi = new GoogleCalendar();
    Events listaeventosapi = new Events();
    Collection<Evento> listaeventosdb = null;

    /**
     * Crea un nuevo evento con todos los parámetros indicados. También es
     * guardado en internet y en la base de datos.
     *
     * @param id
     * @param status
     * @param updated
     * @param nombre
     * @param description
     * @param location
     * @param color
     * @param startdate
     * @param startdatetime
     * @param starttimezone
     * @param enddate
     * @param enddatetime
     * @param endtimezone
     * @param recurrence
     * @param recurrenceeventid
     * @param originalstartdate
     * @param originalstartdatetime
     * @param originalstartdatetimezone
     * @param transparency
     * @param visibility
     * @param icaluid
     * @param sequencia
     * @param reminderdefault
     * @param remindermethod
     * @param reminderminutes
     * @param calendario
     * @return El Evento creado
     * @throws SQLIntegrityConstraintViolationException
     * @throws IOException
     * @throws SQLException
     */
    @Override
    public Evento nuevoevento(String id, String status, Date updated, String nombre, String description,
            String location, String color, Date startdate, Date startdatetime, String starttimezone,
            Date enddate, Date enddatetime, String endtimezone, String recurrence, String recurrenceeventid,
            Date originalstartdate, Date originalstartdatetime, String originalstartdatetimezone,
            String transparency, String visibility, String icaluid, String sequencia,
            Boolean reminderdefault, String remindermethod, int reminderminutes,
            Calendario calendario) throws SQLIntegrityConstraintViolationException, IOException, SQLException {
        Evento evento = new Evento(id, updated, startdate, startdatetime);
        evento.setStatus(status);
        evento.setSummary(nombre);
        evento.setDescription(description);
        evento.setLocation(location);
        evento.setColorid(color);
        evento.setStarttimezone(starttimezone);
        evento.setEnddate(enddate);
        evento.setEnddatetime(enddatetime);
        evento.setEndtimezone(endtimezone);
        evento.setRecurrence(recurrence);
        evento.setRecurrenceeventid(recurrenceeventid);
        evento.setOriginalstartdate(originalstartdate);
        evento.setOriginalstartdatetime(originalstartdatetime);
        evento.setOriginaltimezone(originalstartdatetimezone);
        evento.setTransparency(transparency);
        evento.setVisibility(visibility);
        evento.setIcaluid(icaluid);
        evento.setSequencia(sequencia);
        evento.setRemindersdefault(reminderdefault);
        evento.setRemindermethod(remindermethod);
        evento.setReminderminutes(reminderminutes);
        evento.setCalendario(calendario);
        eventoDao.guardar(evento);
        googleapi.nuevoEvento(nombre, startdate, enddate, calendario.getId());
        return evento;
    }

    /**
     * Crea un nuevo evento con los parámetros indicados También lo guarda en
     * internet y en la base de datos
     *
     * @param status
     * @param nombre
     * @param description
     * @param location
     * @param color
     * @param startdate
     * @param startdatetime
     * @param starttimezone
     * @param enddate
     * @param enddatetime
     * @param endtimezone
     * @param calendario
     * @return el Evento nuevo
     * @throws SQLIntegrityConstraintViolationException
     * @throws IOException
     * @throws SQLException
     * @throws Exception
     */
    public Evento nuevoevento(String status, String nombre, String description,
            String location, String color, Date startdate, Date startdatetime, String starttimezone,
            Date enddate, Date enddatetime, String endtimezone, String recurrence, Calendario calendario) throws SQLIntegrityConstraintViolationException, IOException, SQLException, Exception {
        String usuario = calendario.getCuenta().getUsuario().getNombre();
        System.out.println("Usuario: " + usuario);
        this.googleapi.iniciarSesion(usuario);
        Evento evento = new Evento();
        evento.setStartdate(startdate);
        evento.setStartdatetime(startdatetime);
        evento.setEnddate(enddate);
        evento.setEnddatetime(enddatetime);
        evento.setStatus(status);
        evento.setSummary(nombre);
        evento.setDescription(description);
        evento.setLocation(location);
        evento.setColorid(color);
        evento.setCreated(new Date());
        evento.setStarttimezone(starttimezone);
        
        evento.setEndtimezone(endtimezone);
        evento.setRecurrence(recurrence);
        evento.setCalendario(calendario);
        String idCalendario = calendario.getId();
        Event ev = googleapi.nuevoEvento(this.convertir(evento), idCalendario.substring(0, idCalendario.length() - 1));
        evento.setId(ev.getId());
        eventoDao.guardar(evento);
        return evento;
    }

    /**
     * Carga desde la base de datos los eventos correspondientes a un Calendario
     *
     * @param id
     * @return una lista de eventos
     * @throws SQLException
     */
    @Override
    public Collection<Evento> cargareventosDb(String id) throws SQLException {
        Calendario cal = new Calendario();
        cal.setId(id);
        listaeventosdb = eventoDao.cargar(cal);
        return listaeventosdb;
    }

    /**
     * Filtra los eventos correspondientes a una fecha y un calendario. En caso
     * de ya tener cargada en listaeventosdb el calendario correspondiente sólo
     * filtra los eventos por la fecha. En caso contrario, carga dicho
     * calendario en listaeventosdb y luego filtra
     *
     * @param id
     * @param fecha
     * @return una lista con los eventos de tal fecha
     * @throws SQLException
     */
    @Override
    public Collection<Evento> cargareventos(String id, Date fecha) throws SQLException {
        Calendario cal = new Calendario();
        cal.setId(id);
        try {
            if (!listaeventosdb.iterator().next().getCalendario().getId().equals(id)) {
                listaeventosdb = eventoDao.cargar(cal);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        if (listaeventosdb == null) {
            listaeventosdb = eventoDao.cargar(cal);
        }

        Collection<Evento> listaTemp = new ArrayList<>();
        for (Evento next : listaeventosdb) {
            if (next.getStartdate() == fecha) {
                listaTemp.add(next);
            }
        }
        return listaTemp;
    }

    /**
     * Obtiene desde internet los eventos correspondientes a un calendario
     *
     * @param idCalendario
     * @return una lista con Events
     * @throws IOException
     * @throws Exception
     */
    public Events cargarEventosApi(String idCalendario) throws IOException, Exception {
        listaeventosapi = googleapi.getEventos(idCalendario);
        return listaeventosapi;
    }

    /**
     * Actualiza un evento preexistente en internet y en la base de datos
     *
     * @param id
     * @param status
     * @param updated
     * @param nombre
     * @param description
     * @param location
     * @param color
     * @param startdate
     * @param startdatetime
     * @param starttimezone
     * @param enddate
     * @param enddatetime
     * @param endtimezone
     * @param recurrence
     * @param recurrenceeventid
     * @param originalstartdate
     * @param originalstartdatetime
     * @param originalstartdatetimezone
     * @param transparency
     * @param visibility
     * @param icaluid
     * @param sequencia
     * @param reminderdefault
     * @param remindermethod
     * @param reminderminutes
     * @param calendario
     * @throws SQLIntegrityConstraintViolationException
     * @throws SQLException
     */
    @Override
    public void actualizar(String id, String status, Date updated, String nombre, String description,
            String location, String color, Date startdate, Date startdatetime, String starttimezone,
            Date enddate, Date enddatetime, String endtimezone, String recurrence, String recurrenceeventid,
            Date originalstartdate, Date originalstartdatetime, String originalstartdatetimezone,
            String transparency, String visibility, String icaluid, String sequencia,
            Boolean reminderdefault, String remindermethod, int reminderminutes,
            Calendario calendario) throws SQLIntegrityConstraintViolationException, SQLException {

        Evento evento = new Evento(id, updated, startdate, startdatetime);
        evento.setStatus(status);
        evento.setSummary(nombre);
        evento.setDescription(description);
        evento.setLocation(location);
        evento.setColorid(color);
        evento.setStarttimezone(starttimezone);
        evento.setEnddate(enddate);
        evento.setEnddatetime(enddatetime);
        evento.setEndtimezone(endtimezone);
        evento.setRecurrence(recurrence);
        evento.setRecurrenceeventid(recurrenceeventid);
        evento.setOriginalstartdate(originalstartdate);
        evento.setOriginalstartdatetime(originalstartdatetime);
        evento.setOriginaltimezone(originalstartdatetimezone);
        evento.setTransparency(transparency);
        evento.setVisibility(visibility);
        evento.setIcaluid(icaluid);
        evento.setSequencia(sequencia);
        evento.setRemindersdefault(reminderdefault);
        evento.setRemindermethod(remindermethod);
        evento.setReminderminutes(reminderminutes);
        evento.setCalendario(calendario);
        try {
            googleapi.modificarEvento(this.convertir(evento),new Calendar().set("Id", calendario.getId()));
            eventoDao.guardar(evento);
        } catch (IOException ex) {
            Logger.getLogger(EventoService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Borra un evento de internet de de la base de datos
     *
     * @param evento
     * @throws IOException
     */
    @Override
    public void borrar(Evento evento) throws IOException {
        eventoDao.borrar(evento.getId());
        googleapi.eliminarEvento(evento.getId());
    }

    /**
     * Obtiene los eventos de internet, y compara con los que ya existen en la
     * base de datos, guardando los que no se encuentren en la base de datos y
     * actualizando los que correspondan
     *
     * @param calendario
     * @throws IOException
     * @throws SQLIntegrityConstraintViolationException
     * @throws SQLException
     * @throws Exception
     */
    public void sincronizar(Calendario calendario) throws IOException, SQLIntegrityConstraintViolationException, SQLException, Exception {
        Collection<Evento> listaLocal = this.cargareventosDb(calendario.getId());
        String usuario = calendario.getCuenta().getUsuario().getNombre();
        this.googleapi.iniciarSesion(usuario);
        List<Event> listaRemota = null;
        try {
            listaRemota = this.cargarEventosApi(calendario.getId().substring(0, calendario.getId().length() - 1)).getItems();
            for (Event next : listaRemota) {
                if (null != next.getSummary()) {
                    try {
                        Iterator<Evento> iteraLocal = listaLocal.iterator();
                        Evento evento;
                        boolean pertenece = false;
                        while (iteraLocal.hasNext()) {
                            evento = iteraLocal.next();
                            if (evento.getId().equals(next.getId())) {
                                Evento eventoTemp = this.convertir(next);
                                eventoTemp.setCalendario(calendario);
                                if (!evento.equals(eventoTemp)) {
                                    eventoDao.actualizar(eventoTemp);
                                }
                                pertenece = true;
                            }
                        }
                        if (!pertenece) {
                            Evento eventoTemp = this.convertir(next);
                            if (calendario.getId().contains("#contacts")|calendario.getId().contains("es.ar#holiday")){
                            eventoTemp.setStartdate(new Date(eventoTemp.getStartdate().getTime()+86400000));
                            eventoTemp.setEnddate(new Date(eventoTemp.getEnddate().getTime()+86400000));
                            eventoTemp.setStartdatetime(new Date(eventoTemp.getStartdatetime().getTime()+10800000));
                            eventoTemp.setEnddatetime(new Date(eventoTemp.getEnddatetime().getTime()+10800000));
                            }
                            eventoTemp.setCalendario(calendario);
                            eventoDao.guardar(eventoTemp);

                        }
                    } catch (java.util.NoSuchElementException ex) {
                        Evento eventoTemp = this.convertir(next);
                        eventoTemp.setCalendario(calendario);
                        eventoDao.guardar(eventoTemp);

                    }
                }

            }
        } catch (IOException ex) {
            System.err.println("Error en la carga de los eventos desde internet.");
            System.err.println(ex.getStackTrace());
        }
    }

    /**
     * Realiza la conversión de un Event de la API de google a un Evento
     *
     * @param ev Event
     * @return el Evento con los mismo valores
     */
    public Evento convertir(Event ev) {
        Evento evento = new Evento(ev.getId());

        //new Evento(id, created, startdate, startdatetime)
        try {
            if (ev.getId().contains("#contacts") |ev.getId().contains("es.ar#holiday")){
             evento.setStartdate(new java.sql.Date(ev.getStart().getDateTime().getValue()+86400000));
            evento.setStartdatetime(new java.sql.Date(ev.getStart().getDateTime().getValue()+10800000));
            }
            else{
            evento.setStartdate(new java.sql.Date(ev.getStart().getDateTime().getValue()));
            evento.setStartdatetime(new java.sql.Date(ev.getStart().getDateTime().getValue()));}
        } catch (Exception e) {
             
            evento.setStartdate(new java.sql.Date(ev.getStart().getDate().getValue()));
            evento.setStartdatetime(new java.sql.Date(ev.getStart().getDate().getValue()));}
        

        try {
            evento.setCreated(new java.sql.Date(ev.getCreated().getValue()));
        } catch (Exception e) {
            evento.setCreated(evento.getStartdate());
        }
        evento.setStarttimezone(ev.getStart().getTimeZone());

        try {
            evento.setEnddate(new java.sql.Date(ev.getEnd().getDateTime().getValue()));
            evento.setEnddatetime(new java.sql.Date(ev.getEnd().getDateTime().getValue()));
        } catch (Exception e) {
            evento.setEnddate(new java.sql.Date(ev.getEnd().getDate().getValue()));
            evento.setEnddatetime(new java.sql.Date(ev.getEnd().getDate().getValue()));

        }

        evento.setEndtimezone(ev.getEnd().getTimeZone());

        try {
            evento.setOriginalstartdate(new java.sql.Date(ev.getStart().getDateTime().getValue()));
            evento.setOriginalstartdatetime(new java.sql.Date(ev.getStart().getDateTime().getValue()));
        } catch (Exception e) {
            evento.setOriginalstartdate(new java.sql.Date(ev.getStart().getDate().getValue()));
            evento.setOriginalstartdatetime(new java.sql.Date(ev.getStart().getDate().getValue()));
        }
        evento.setOriginaltimezone(ev.getStart().getTimeZone());

        evento.setStatus(ev.getStatus());
        evento.setSummary(ev.getSummary());
        if (ev.getDescription() != null) {
            if (ev.getDescription().length() > 255) {
                evento.setDescription(ev.getDescription().substring(0, 255));
            } else {
                evento.setDescription(ev.getDescription());
            }
        }
        evento.setLocation(ev.getLocation());
        evento.setColorid(ev.getColorId());
        evento.setRecurrence("1");
        evento.setRecurrenceeventid("null");
        evento.setTransparency(ev.getTransparency());
        evento.setVisibility(ev.getVisibility());
        evento.setIcaluid(ev.getICalUID());
        evento.setSequencia(ev.getSequence().toString());
        //evento.setRemindersdefault(ev.getReminders().getUseDefault().booleanValue());
        return evento;
    }

    /**
     * Realiza la conversión de un Evento en un Event
     *
     * @param ev
     * @return
     */
    public Event convertir(Evento ev) {
        Event event = new Event();
        event.setSummary(ev.getSummary());
        event.setStatus(ev.getStatus());
        event.setStart(new EventDateTime().setDateTime(new DateTime(ev.getStartdatetime(), TimeZone.getDefault())));
        event.setEnd(new EventDateTime().setDateTime(new DateTime(ev.getEnddatetime(), TimeZone.getDefault())));
        event.setCreated(new DateTime(ev.getCreated()));
        event.setDescription(ev.getDescription());
        event.setLocation(ev.getLocation());
        event.setColorId(ev.getColorid());
        event.setRecurrence(Arrays.asList(ev.getRecurrence()));
        return event;
    }

    /**
     * Cuenta la cantidad de eventos que hay en una fecha en un determinado
     * calendario
     *
     * @param fecha
     * @param idCalendario
     * @return la cantidad de eventos en dicha fecha
     * @throws SQLException
     */
    public int cantidadEventos(Date fecha, String idCalendario) throws SQLException {
        int count = 0;
        Collection<Evento> lista = eventoDao.cargaPorFecha(idCalendario, fecha);
        if (!lista.isEmpty()) {
            for (Evento next : lista) {
                if (next.getStartdate().equals(new java.sql.Date(fecha.getTime()))) {
                    count = count + 1;
                }
            }
        }
        return count;
    }
}
