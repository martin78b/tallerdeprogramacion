/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import api.GoogleCalendar;
import dao.CalendarioDao;
import entidades.Calendario;
import entidades.Cuenta;
import java.util.Collection;
import com.google.api.services.calendar.model.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Martin
 */
public class CalendarioService implements ICalendarioService {

    CalendarioDao calDao = new CalendarioDao();
    GoogleCalendar googleapi = new GoogleCalendar();
    Collection<Calendario> listaCalendariosDb;
    List<CalendarListEntry> listaCalendariosApi;
    Cuenta cuenta;

    /**
     * Crea una nueva entidad de Calendario, y la guarda en la base de datos y
     * en internet
     *
     * @param id El id correspondiente al calendario
     * @param nombre Nombre que tenga el calendario
     * @param descripcion Una descripcion del calendario
     * @param ubicacion Ubicacion geográfica
     * @param timezone Zona horaria
     * @param tipo Tipo de calendario
     * @param cuenta La cuenta a la cuál corresponde este calendario
     * @return el nuevo Calendario
     * @throws IOException
     */
    @Override
    public Calendario crear(String id, String nombre, String descripcion,
            String ubicacion, String timezone, String tipo, Cuenta cuenta) throws IOException {
        Calendario cal = new Calendario();
        cal.setId(id);
        cal.setCuenta(cuenta);
        cal.setKind(tipo);
        cal.setDescription(descripcion);
        cal.setTomezone(timezone);
        cal.setSummary(nombre);
        cal.setLocation(ubicacion);
        Calendar calendar = googleapi.agregarCalendario(this.convertir(cal));
        cal.setId(calendar.getId()+cuenta.getId());
        calDao.guardar(cal);
        return cal;
    }

    /**
     * Carga una colección con los calendarios que corresponden a una Cuenta
     *
     * @param id el ID de la cuenta a la corresponden los calendarios
     * @return Una colección con los Calendarios de la cuenta
     */
    @Override
    public Collection<Calendario> cargar(String id) {
        listaCalendariosDb = calDao.cargar(new Cuenta(id));
        return listaCalendariosDb;
    }

    /**
     * Carga los calendarios de la cuenta actual desde Internet
     *
     * @return Una lista con los calendarios de dicha cuenta
     * @throws IOException
     */
    public List<CalendarListEntry> cargarApi() throws IOException, Exception {
        List<CalendarListEntry> lista = googleapi.getCalendarios();
        listaCalendariosApi = new ArrayList<>();
        for (CalendarListEntry calendarListEntry : lista) {
            if (calendarListEntry.getId().equals(null)) {
                //no haga nada
            } else {
                listaCalendariosApi.add(calendarListEntry);
            }
        }
        return listaCalendariosApi;
    }

    /**
     * Sincroniza los calendarios correspondientes a una cuenta con los que 
     * tiene en internet.
     * @param cuentaId
     * @throws IOException
     */
    public void sincronizar(String cuentaId, String usuario) throws IOException, Exception {

        googleapi.iniciarSesion(usuario);
        List<CalendarListEntry> listaWeb = this.cargarApi();
        Collection<Calendario> listaLocal = this.cargar(cuentaId);

        if (listaLocal.isEmpty()) {
            this.sincronizarInternet(cuentaId, listaWeb);
        } else {
            for (CalendarListEntry next : listaWeb) {
                Iterator<Calendario> iteraLocal = listaLocal.iterator();
                Calendario siguiente;
                String idCalendario = next.getId() + cuentaId;
                boolean pertenece = false;
                while (iteraLocal.hasNext()) {
                    siguiente = iteraLocal.next();
                    if (siguiente.getId().contains(next.getId())) {
                        next.setId(idCalendario);
                        pertenece=true;
                        calDao.actualizar(this.convertir(next, cuentaId));
                    }
                    
                }
                if (!pertenece) {
                    System.out.println("Calendario nuevo: "+next.getId());
                    next.setId(idCalendario);
                    calDao.guardar(this.convertir(next, cuentaId));
                }

            }
        }
    }

    /**
     * Al sincronizar con internet copia todos los calendarios y eventos
     * que tenga una cuenta a la base de datos.
     * @param cuentaId
     * @param listaWeb
     * @throws IOException 
     */
    public void sincronizarInternet(String cuentaId, List<CalendarListEntry> listaWeb) throws IOException {
        for (CalendarListEntry next : listaWeb) {
            next.setId(next.getId() + cuentaId);
            calDao.guardar(this.convertir(next, cuentaId));
        }

    }

    /**
     * Actualiza el calendario con el ID indicado, y los nuevos parámetros
     * @param id
     * @param nombre
     * @param descripcion
     * @param ubicacion
     * @param timezone
     * @param tipo
     * @param cuenta 
     */
    @Override
    public void actualizar(String id, String nombre, String descripcion,
            String ubicacion, String timezone, String tipo, Cuenta cuenta) {
        Calendar cal = new Calendar();
        cal.setId(id);
        cal.setKind(tipo);
        cal.setDescription(descripcion);
        cal.setTimeZone(timezone);
        cal.setSummary(nombre);
        cal.setLocation(ubicacion);
        calDao.actualizar(this.convertir(cal));
        try {
            googleapi.modificarCalendario(cal);
        } catch (IOException ex) {
            Logger.getLogger(CalendarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Borra un calendario de internet y de la base de datos.
     * @param idCalendario
     * @param usuario
     * @param cuenta
     * @throws IOException 
     */
    @Override
    public void borrar(String idCalendario, String usuario, Cuenta cuenta) throws IOException {
        Calendario cal = new Calendario();
        cal.setId(idCalendario);
        calDao.borrar(cal);
        try {
            googleapi.iniciarSesion(usuario);
        } catch (Exception ex) {
            Logger.getLogger(CalendarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Borrado calendario: "+idCalendario);
        int largo = idCalendario.length();
        System.out.println("De la API: "+idCalendario.substring(0, largo-1));
        googleapi.eliminarCalendario(idCalendario.substring(0, largo-1));
    }

    /**
     * Realiza una conversión de un CalendarListEntry en Calendario
     * @param cal
     * @param idCuenta
     * @return un Calendario
     */
    public Calendario convertir(CalendarListEntry cal, String idCuenta) {
        Calendario calendario = new Calendario();
        calendario.setId(cal.getId());
        calendario.setSummary(cal.getSummary());
        calendario.setDescription(cal.getDescription());
        calendario.setKind(cal.getKind());
        calendario.setTomezone(cal.getTimeZone());
        calendario.setLocation(cal.getLocation());

        calendario.setCuenta(new Cuenta(idCuenta));
        return calendario;
    }

    /**
     * Realiza la conversión de un Calendar en Calendario
     * @param cal
     * @return 
     */
    public Calendario convertir(Calendar cal) {
        Calendario calendario = new Calendario();
        calendario.setId(cal.getId());
        calendario.setSummary(cal.getSummary());
        calendario.setDescription(cal.getDescription());
        calendario.setKind(cal.getKind());
        calendario.setTomezone(cal.getTimeZone());
        calendario.setLocation(cal.getLocation());
        return calendario;
    }

    /**
     * Convierte un Calendario en Calendar de internet.
     * @param cal
     * @return 
     */
    public Calendar convertir(Calendario cal) {
        Calendar calendar = new Calendar();
        calendar.setId(cal.getId());
        calendar.setSummary(cal.getSummary());
        calendar.setDescription(cal.getDescription());
        calendar.setKind(cal.getKind());
        calendar.setTimeZone(cal.getTomezone());
        calendar.setLocation(cal.getLocation());
        return calendar;
    }
}
