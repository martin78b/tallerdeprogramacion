/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import entidades.Calendario;
import entidades.Evento;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author Mauricio
 */
public interface IEventoServise {

public Evento nuevoevento(String id, String status, Date updated, String nombre, String description,
            String location, String color, Date startdate, Date startdatetime, String starttimezone,
            Date enddate, Date enddatetime, String endtimezone, String recurrence, String recurrenceeventid,
            Date originalstartdate, Date originalstartdatetime, String originalstartdatetimezone,
            String transparency, String visibility, String icaluid, String sequencia,
            Boolean reminderdefault, String remindermethod, int reminderminutes,
            Calendario calendario) throws SQLIntegrityConstraintViolationException, IOException, SQLException ;

public Collection<Evento> cargareventosDb(String id) throws SQLException ;

public Collection<Evento> cargareventos(String id, Date fecha) throws SQLException;

public void actualizar(String id, String status, Date updated, String nombre, String description,
            String  location, String color, Date startdate, Date startdatetime, String starttimezone,
            Date enddate, Date enddatetime, String endtimezone, String recurrence, String recurrenceeventid,
            Date originalstartdate, Date originalstartdatetime, String originalstartdatetimezone,
            String transparency, String visibility, String icaluid, String sequencia,
            Boolean reminderdefault, String remindermethod, int reminderminutes, Calendario calendario) throws SQLIntegrityConstraintViolationException, SQLException ;

public void borrar(Evento evento) throws IOException;

}
