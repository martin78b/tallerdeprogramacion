/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import entidades.Calendario;
import entidades.Cuenta;
import java.io.IOException;
import java.util.Collection;

/**
 *
 * @author Martin
 */
public interface ICalendarioService {
    
    public Calendario crear(String id, String nombre, String descripcion, String ubicacion, String timezone, String tipo, Cuenta cuenta) throws IOException;
    
    public Collection<Calendario> cargar(String id);
    
    public void actualizar(String id, String nombre, String descripcion, String ubicacion, String timezone, String tipo, Cuenta cuenta);
    
    public void borrar(String idCalendario, String usuario, Cuenta cuenta) throws IOException;
}
