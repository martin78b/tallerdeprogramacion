/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import entidades.Cuenta;
import java.util.Collection;

/**
 *
 * @author Martin
 */
public interface ICuentaService {
    
    public Cuenta crear(String email, String nombre, String servicio, String token, String usuario);
    
    public Collection<Cuenta> cargar(String nombre);
    
    public void eliminarCuenta(String nombre);
    
    public void modificarCuenta(String email, String nombre, String servicio, String token);
    
}
