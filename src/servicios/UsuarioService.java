/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import dao.UsuarioDAO;
import entidades.Usuario;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Mauricio
 */
public class UsuarioService {

    UsuarioDAO usuariodao = new UsuarioDAO();
    Usuario usuario;

    public void crearUsuario(String username, String pass) throws SQLIntegrityConstraintViolationException {
        Usuario user = new Usuario();
        user.setNombre(username);
        user.setPassword(pass);
        usuariodao.guardar(user);

    }

    public void actualizarUsuario(String username, String password) {
        Usuario user = new Usuario();
        user.setNombre(username);
        user.setPassword(password);
        usuariodao.actualizar(user);
    }

    public Usuario cargar(String username) throws SQLException {
        usuario = usuariodao.cargar(username);
        return usuario;

    }
    
    public boolean verificar(String user, String pass) throws SQLException{
        boolean coinciden=false;
        try{
        Usuario user1=this.cargar(user);
        if(user1.getPassword().equals(pass)){
            coinciden=true;
        } else {
            coinciden=false;
        }
        } catch(NullPointerException ex){
            coinciden=false;
        }
        return coinciden;
    }
    
    public void borrar(Usuario user){
        usuariodao.borrar(user);
    }
    
    public Usuario consultar(){
        return usuario;
    }

}
