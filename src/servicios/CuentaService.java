/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import api.GoogleCalendar;
import dao.CuentaDAO;
import entidades.Cuenta;
import java.io.IOException;
import java.util.Collection;

/**
 *
 * @author Martin
 */
public class CuentaService implements ICuentaService {

    CuentaDAO cuentaDao = new CuentaDAO();
    GoogleCalendar apiGoogle = new GoogleCalendar();
    CalendarioService calService = new CalendarioService();
    Cuenta cuenta;

    @Override
    public Cuenta crear(String email, String nombre, String servicio, String token, String usuario) {
        cuenta.setEmail(email);
        cuenta.setDisplayname(nombre);
        cuenta.setServicio(servicio);
        cuenta.setToken(token);
        cuentaDao.guardar(email, nombre, servicio, token, usuario);
        return cuenta;
    }

    @Override
    public Collection<Cuenta> cargar(String nombre) {
        //cuenta.setDisplayname(nombre);
        return cuentaDao.cargar(nombre);
    }

    @Override
    public void eliminarCuenta(String nombre) {
        Cuenta cuenta = new Cuenta();
        cuenta.setDisplayname(nombre);
        cuentaDao.borrar(cuenta);
    }

    @Override
    public void modificarCuenta(String email, String nombre, String servicio, String token) {
        Cuenta cuenta = new Cuenta();
        cuenta.setDisplayname(nombre);
        cuenta.setEmail(email);
        cuenta.setServicio(servicio);
        cuenta.setToken(token);
        cuentaDao.modificar(cuenta);
    }

    public void autenticar(String usuario) throws IOException, Exception {
        //apiGoogle.start();
        apiGoogle.iniciarSesion(usuario);
        //this.crear(apiGoogle.dato().getEmail(), apiGoogle.dato().getDisplayname(),
        //       apiGoogle.dato().getServicio(), apiGoogle.dato().getToken());
        Cuenta cuenta = apiGoogle.dato();

        cuentaDao.guardar(cuenta.getEmail(), cuenta.getDisplayname(), cuenta.getServicio(), cuenta.getToken(), usuario);

        //System.out.println(cuenta.getId());

        //calService.sincronizarInternet("2", apiGoogle.getCalendarios());
    }
}
