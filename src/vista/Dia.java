/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.DefaultListModel;
import servicios.EventoService;
import entidades.Calendario;
import entidades.Evento;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Mauricio
 */
public class Dia extends javax.swing.JFrame {

    vista.Calendario calen;
    DefaultListModel<String> listModel = new DefaultListModel<>();
    Calendario calendar;
    EventoService eventoServ;
    Date fechaElegida;
    Collection<Evento> listaCargada;

    /**
     * Crea el formulario de los eventos del Día seleccionado en la ventana
     * Calendario
     *
     * @param fecha indica la fecha seleccionada para cargar los eventos
     * @param evService es el service de evento con el que se trabaja
     * @param calendario es el calendario al que corresponden los eventos
     */
    public Dia(Calendario calendario, EventoService evService, Date fecha, vista.Calendario cal) {
        
        calen = cal;
        calendar = calendario;
        eventoServ = evService;
        fechaElegida = fecha;
        try {
            //jLabel1.setText("Calendario: "+calendario.getSummary());
            listaCargada = evService.cargareventosDb(calendario.getId());
            for (Iterator<Evento> iterator = listaCargada.iterator(); iterator.hasNext();) {
                Evento siguiente = iterator.next();
                if (siguiente.getStartdate().compareTo(fecha) == 0) {
                    if (siguiente.getStartdatetime().equals(siguiente.getEnddatetime())) {
                        listModel.addElement(siguiente.getSummary() + "    Todo el día   " + ((siguiente.getLocation() == null) ? "" : siguiente.getLocation()));

                    } else {
                        listModel.addElement(siguiente.getSummary() + "   " + siguiente.getStartdatetime().toString().substring(0, 5) + "-" + siguiente.getEnddatetime().toString().substring(0, 5) + "   " + ((siguiente.getLocation() == null) ? "" : siguiente.getLocation()));
                    }
                }
            }

            this.setVisible(true);
            initComponents();
        } catch (SQLException ex) {
            Logger.getLogger(Dia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jButton2 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Vista del día");

        jButton1.setText("Modificar evento");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jList1.setModel(listModel);
        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList1);

        jButton2.setText("Eliminar Evento");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel1.setText("Calendario");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)))
                        .addGap(0, 17, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(25, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
  Calendar c = Calendar.getInstance();
        c.setTime(fechaElegida);
        Evento ev = null;
        for (Evento evento : listaCargada) { if (jList1.getSelectedValue().contains(evento.getSummary())){
        ev = evento;
        }
            
        }
        ModificarEvento modev = new ModificarEvento(ev, eventoServ, calen);
        modev.setTitle("Modificar Evento");
        this.dispose();   
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        System.out.println(jList1.isSelectionEmpty());
        if (!jList1.isSelectionEmpty()) {
            for (Evento evento : listaCargada) {
                if(jList1.getSelectedValue().contentEquals(evento.getSummary())){
                    try {
                        eventoServ.borrar(evento);
                        JOptionPane.showMessageDialog(null, "Se eliminó correctamente"
                                + " el evento");
                    } catch (IOException ex) {
                        JOptionPane.showMessageDialog(null, "Ha ocurrido un error "
                                + "al eliminar el evento.");
                        Logger.getLogger(Dia.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione algún evento.");
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Dia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Dia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Dia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Dia.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JList<String> jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
