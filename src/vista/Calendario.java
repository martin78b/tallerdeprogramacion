/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import entidades.Cuenta;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import servicios.CalendarioService;
import servicios.CuentaService;
import servicios.EventoService;
import servicios.UsuarioService;
import utils.Conexion;

/**
 *
 * @author Martin
 */
public class Calendario extends javax.swing.JFrame {

    UsuarioService userService;
    CuentaService cuentService;
    CalendarioService calService;
    EventoService evService;
    String idCalendarioEnUso;

    Collection<entidades.Calendario> listaDeCalendarios;
    ArrayList<String> listaCalendarios = new ArrayList<>();
    Collection<Cuenta> listaDeCuentas;
    ArrayList<String> listaCuentas = new ArrayList<>();

    String calendarioSeleccionado;
    String cuentaSeleccionada;
    Cuenta cuenta;

    /**
     * Se crea la clase Calendario
     *
     * @param userServ
     * @param cuentServ
     * @param calendarServ
     * @param eventoServ
     */
    public Calendario(UsuarioService userServ, CuentaService cuentServ,
            CalendarioService calendarServ, EventoService eventoServ) {
        userService = userServ;
        cuentService = cuentServ;
        calService = calendarServ;
        evService = eventoServ;

        /**
         * Carga la lista de calendarios desde internet y los guarda en base de
         * datos, junto con los eventos.
         */
        listaDeCuentas = cuentService.cargar(userService.consultar().getNombre());
        cuentaSeleccionada = listaDeCuentas.iterator().next().getId();
        for (entidades.Cuenta next : listaDeCuentas) {
            listaCuentas.add(next.getDisplayname());
            if(cuentaSeleccionada.equals(next.getId())){
                cuenta = next;
            }
        }

        /**
         * De haber internet se sincronizan los calendarios de la cuenta
         */
        this.sincronizar();
             
        this.setVisible(true);
        this.mtblCalendar = new DefaultTableModel() {
            public boolean isCellEditable(int rowIndex, int mColIndex) {
                return false;
            }
        };
        stblCalendar = new JScrollPane(jTable1);

        /**
         * Se obtiene el mes y el año reales
         */
        GregorianCalendar cal = new GregorianCalendar(); //Create calendar
        realDay = cal.get(GregorianCalendar.DAY_OF_MONTH); //Get day
        realMonth = cal.get(GregorianCalendar.MONTH); //Get month
        realYear = cal.get(GregorianCalendar.YEAR); //Get year
        actualMes = realMonth; //Match month and year
        actualAnio = realYear;

        //Add headers
        String[] headers = {"Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"}; //All headers
        for (int i = 0; i < 7; i++) {
            mtblCalendar.addColumn(headers[i]);
        }
        mtblCalendar.setColumnCount(7);
        mtblCalendar.setRowCount(6);

        initComponents();
        actualizarCalendario(realMonth, realYear);
    }

    /**
     * Actualiza la vista del calendario gráfico, para el mes y el año indicados
     *
     * @param mes
     * @param anio
     */
    public void actualizarCalendario(int mes, int anio) {
        //Variables
        String[] meses = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
        int nod, som; //Number Of Days, Start Of Month
        lblMes.setText(meses[mes]);
        comboAnio.setSelectedItem(String.valueOf(anio));

        //Limpiar la tabla
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j++) {
                mtblCalendar.setValueAt(null, i, j);
            }
        }

        //Get first day of month and number of days
        GregorianCalendar cal = new GregorianCalendar(anio, mes, 1);
        nod = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        som = cal.get(GregorianCalendar.DAY_OF_WEEK);

        //Hacer el calendario
        for (int i = 1; i <= nod; i++) {
            try {
                int fila = new Integer((i + som - 2) / 7);
                int columna = (i + som - 2) % 7;
                int numero = 0;
                numero = (evService.cantidadEventos(
                        new Date(new GregorianCalendar(anio, mes, i).getTimeInMillis()),
                        idCalendarioEnUso));
                if (idCalendarioEnUso.contains("#contacts") | idCalendarioEnUso.contains("es.ar#holiday")){
                    jButton1.setEnabled(false);
                    } else {jButton1.setEnabled(true);
                    }
                String cantEventos = "(" + numero + ")";
                if (cantEventos.equals("(0)")) {
                    cantEventos = "";
                }
                String info = i + " " + cantEventos;
                mtblCalendar.setValueAt(info, fila, columna);
            } catch (NoSuchElementException | SQLException ex) {
                Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    /**
     * Realiza la sincronización de los calendarios con internet
     */
    private void sincronizar() {
        boolean hayInternet = utils.Conexion.hayInternet();
        if (hayInternet) {
            try {
                for (entidades.Cuenta next : listaDeCuentas) {
                    calService.sincronizar(next.getId(), userService.consultar().getNombre());
                }
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Falló la sincronización  de Calendarios con internet.");

                System.err.println("Falló la sincronización  de Calendarios con internet.");
                Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        listaDeCalendarios = calService.cargar(cuentService.cargar(userService
                .consultar().getNombre()).iterator().next().getId());
        idCalendarioEnUso = listaDeCalendarios.iterator().next().getId();

        if (hayInternet) {
            for (entidades.Calendario listaDeCalendario : listaDeCalendarios) {
                try {
                    evService.sincronizar(listaDeCalendario);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Problema de entrada/salida, al guardar un evento.");
                    System.err.println("Problema de entrada/salida, al guardar un evento.");
                    Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null, "Problema al guardar evento/s en la base de datos.");
                    System.err.println("Problema al guardar evento/s en la base de datos.");
                    Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(Calendario.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        /**
         * Arma la lista de los calendarios del usuario
         */
        for (entidades.Calendario next : listaDeCalendarios) {
            listaCalendarios.add(next.getSummary());
        
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        lblMes = new javax.swing.JLabel();
        comboAnio = new javax.swing.JComboBox<>();
        btnAdminCAlendarios = new javax.swing.JButton();
        jComboCal = new javax.swing.JComboBox<>();
        jComboCuentas = new javax.swing.JComboBox<>();
        sincronizarB = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Kalendaro");
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnAnterior.setText("Anterior");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });

        btnSiguiente.setText("Siguiente");
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
        jTable1.setModel(mtblCalendar);
        jTable1.setToolTipText("Doble click para ver la información del día");
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTable1.setRowHeight(66);
        jTable1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable1.setShowHorizontalLines(true);
        jTable1.setShowVerticalLines(true);
        jTable1.getTableHeader().setResizingAllowed(false);
        jTable1.getTableHeader().setReorderingAllowed(false);
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dobleclik(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        lblMes.setText("Mes");

        comboAnio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "2116", "2115", "2114", "2113", "2112", "2111", "2110", "2109", "2108", "2107", "2106", "2105", "2104", "2103", "2102", "2101", "2100", "2099", "2098", "2097", "2096", "2095", "2094", "2093", "2092", "2091", "2090", "2089", "2088", "2087", "2086", "2085", "2084", "2083", "2082", "2081", "2080", "2079", "2078", "2077", "2076", "2075", "2074", "2073", "2072", "2071", "2070", "2069", "2068", "2067", "2066", "2065", "2064", "2063", "2062", "2061", "2060", "2059", "2058", "2057", "2056", "2055", "2054", "2053", "2052", "2051", "2050", "2049", "2048", "2047", "2046", "2045", "2044", "2043", "2042", "2041", "2040", "2039", "2038", "2037", "2036", "2035", "2034", "2033", "2032", "2031", "2030", "2029", "2028", "2027", "2026", "2025", "2024", "2023", "2022", "2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "2011", "2010", "2009", "2008", "2007", "2006", "2005", "2004", "2003", "2002", "2001", "2000", "1999", "1998", "1997", "1996", "1995", "1994", "1993", "1992", "1991", "1990", "1989", "1988", "1987", "1986", "1985", "1984", "1983", "1982", "1981", "1980", "1979", "1978", "1977", "1976", "1975", "1974", "1973", "1972", "1971", "1970", "1969", "1968", "1967", "1966", "1965", "1964", "1963", "1962", "1961", "1960", "1959", "1958", "1957", "1956", "1955", "1954", "1953", "1952", "1951", "1950", "1949", "1948", "1947", "1946", "1945", "1944", "1943", "1942", "1941", "1940", "1939", "1938", "1937", "1936", "1935", "1934", "1933", "1932", "1931", "1930", "1929", "1928", "1927", "1926", "1925", "1924", "1923", "1922", "1921", "1920", "1919", "1918", "1917", "1916", "1915", "1914", "1913", "1912", "1911", "1910", "1909", "1908", "1907", "1906", "1905", "1904", "1903", "1902", "1901", "1900" }));
        comboAnio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAnioActionPerformed(evt);
            }
        });

        btnAdminCAlendarios.setText("Administrar Calendarios");
        btnAdminCAlendarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdminCAlendariosActionPerformed(evt);
            }
        });

        jComboCal.setModel(new DefaultComboBoxModel(listaCalendarios.toArray()));
        jComboCal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboCalActionPerformed(evt);
            }
        });

        jComboCuentas.setModel(new DefaultComboBoxModel(listaCuentas.toArray()));
        jComboCuentas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboCuentasActionPerformed(evt);
            }
        });

        sincronizarB.setText("Sincronizar");
        sincronizarB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sincronizarBActionPerformed(evt);
            }
        });

        jButton1.setText("Crear evento");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Cuenta:");

        jButton2.setText("Cuentas");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboCal, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnAdminCAlendarios, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(sincronizarB)
                        .addGap(0, 12, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAnterior)
                        .addGap(38, 38, 38)
                        .addComponent(lblMes)
                        .addGap(34, 34, 34)
                        .addComponent(btnSiguiente))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(comboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(213, 213, 213))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboCuentas, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sincronizarB)
                    .addComponent(jLabel1)
                    .addComponent(btnAdminCAlendarios)
                    .addComponent(jButton2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSiguiente)
                    .addComponent(btnAnterior)
                    .addComponent(lblMes)
                    .addComponent(jComboCal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(40, 40, 40)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboAnio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Boton de mes anterior Realiza la carga para el mes anterior al que estaba
     *
     * @param evt
     */
    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        if (actualMes == 0) {
            actualMes = 11;
            actualAnio -= 1;
        } else {
            actualMes -= 1;
        }
        actualizarCalendario(actualMes, actualAnio);
    }//GEN-LAST:event_btnAnteriorActionPerformed

    /**
     * Combo de selección del año Al cambiar el año actualiza la vista del
     * calendario al año y mes correspondientes
     *
     * @param evt
     */
    private void comboAnioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAnioActionPerformed
        if (comboAnio.getSelectedItem() != null) {
            String b = comboAnio.getSelectedItem().toString();
            actualAnio = Integer.parseInt(b);
            actualizarCalendario(actualMes, actualAnio);
        }
    }//GEN-LAST:event_comboAnioActionPerformed

    /**
     * Botón mes siguiente cambia al mes siguiente del actual y actualiza la
     * vista de calendario
     *
     * @param evt
     */
    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        if (actualMes == 11) {
            actualMes = 0;
            actualAnio += 1;
        } else {
            actualMes += 1;
        }
        actualizarCalendario(actualMes, actualAnio);
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnAdminCAlendariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdminCAlendariosActionPerformed
        AdminCalendarios administarCalendarios = 
                new AdminCalendarios(cuenta, calService, userService.consultar().getNombre());
    }//GEN-LAST:event_btnAdminCAlendariosActionPerformed

    /**
     * Al hacer doble click sobre una de las casillas del calendario se abre una
     * ventana con los eventos correspondientes al día al que corresponde dicha
     * casilla.
     *
     * @param evt
     */
    private void dobleclik(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dobleclik
        if (evt.getClickCount() == 2) {
            JTable target = (JTable) evt.getSource();
            int row = target.getSelectedRow();
            int column = target.getSelectedColumn();
            int datoDia = Integer.parseInt(mtblCalendar.getValueAt(row, column).toString().substring(0, 2).replace(" ", ""));
            entidades.Calendario calendarioCargado = null;
            for (entidades.Calendario next : listaDeCalendarios) {
                if (next.getId().equals(idCalendarioEnUso)) {
                    calendarioCargado = next;
                }
            }

            Dia vistaDia = new Dia(calendarioCargado, evService, new java.sql.Date(
                    new GregorianCalendar(actualAnio, actualMes, datoDia).getTimeInMillis()), this);
            System.out.println(row);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_dobleclik

    /**
     * Combo de selección de calendario Al cambiarla cambia el calendario en
     * uso, y se carga la vista con los eventos del calendario seleccionado
     *
     * @param evt
     */
    private void jComboCalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboCalActionPerformed
        
        if (jComboCal.getSelectedItem() != null) {
            for (entidades.Calendario next : listaDeCalendarios) {
                if (next.getSummary().contains(jComboCal.getSelectedItem().toString())) {
                    idCalendarioEnUso = next.getId();
                    if (idCalendarioEnUso.contains("#contacts") | idCalendarioEnUso.contains("es.ar#holiday")){
                    jButton1.setEnabled(false);
                    } else {jButton1.setEnabled(true);
                    }

                }
            }

        }
        this.actualizarCalendario(actualMes, actualAnio);
    }//GEN-LAST:event_jComboCalActionPerformed

    private void jComboCuentasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboCuentasActionPerformed

    }//GEN-LAST:event_jComboCuentasActionPerformed

    /**
     * Botón para forzar la sincronización En caso de no haber internet, muestra
     * un mensaje.
     *
     * @param evt
     */
    private void sincronizarBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sincronizarBActionPerformed
        if (Conexion.hayInternet()) {
            this.sincronizar();
            actualizarCalendario(actualMes, actualAnio);
        } else {
            JOptionPane.showMessageDialog(null, "No hay conexión a internet para"
                    + " realizar la sincronización.");
        }
    }//GEN-LAST:event_sincronizarBActionPerformed

    /**
     * Botón de nuevo evento Abre una nueva ventana para crear un evento
     *
     * @param evt
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        entidades.Calendario calendarioCargado = null;
        for (entidades.Calendario next : listaDeCalendarios) {
            if (next.getId().equals(idCalendarioEnUso)) {
                calendarioCargado = next;
            }
        }
        NuevoEvento vistaEvnew = new NuevoEvento(calendarioCargado, evService, this, actualMes, actualAnio);
        this.setEnabled(false);
// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        AdminCuentas ventanaCuentas = new AdminCuentas(cuenta, cuentService, 
                userService.consultar().getNombre(), userService, calService, evService);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Calendario.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Calendario.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Calendario.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Calendario.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new Calendario().setVisible(true);
            }
        });

    }

    private final DefaultTableModel mtblCalendar;
    static JScrollPane stblCalendar;
    static int realYear, realMonth, realDay, actualAnio, actualMes;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdminCAlendarios;
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JComboBox<String> comboAnio;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox<String> jComboCal;
    private javax.swing.JComboBox<String> jComboCuentas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel lblMes;
    private javax.swing.JButton sincronizarB;
    // End of variables declaration//GEN-END:variables
}
