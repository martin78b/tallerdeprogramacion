/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import java.awt.FlowLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import servicios.CalendarioService;
import servicios.CuentaService;
import servicios.EventoService;
import servicios.UsuarioService;

/**
 *
 * @author Mauricio
 */
public class IniciarSesionServicio extends JFrame {

    vista.Calendario ventanaCalendario;
    CalendarioService calService;
    EventoService evService;
    CuentaService cuentService;
    UsuarioService userService;
    private JLabel gcalendar;
    private JLabel outlookcalendar;
    private JLabel yahoocalendar;
    private int k;

    public static void main(String args[]) {

        /*IniciarSesionServicio ven = new IniciarSesionServicio();
        ven.setSize(300, 450);
        ven.setVisible(true);
        ven.setLocationRelativeTo(null);
         */
    }

    //public IniciarSesionServicio() {
    //    initComponents();
    //}
    public IniciarSesionServicio(UsuarioService userServ, CuentaService cuentServ,
            CalendarioService calendarServ, EventoService eventoServ) {
        userService = userServ;
        cuentService = cuentServ;
        calService = calendarServ;
        evService = eventoServ;
        initComponents();
        this.setSize(300, 450);
        this.setVisible(true);
        this.setLocationRelativeTo(null);
        //setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt) {
                close();
            }
        });
    }

    private void close() {
        if (JOptionPane.showConfirmDialog(rootPane, "No se agregó ningún servicio ¿Desea realmente salir?",
                "Salir del sistema", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            this.dispose();
        }
    }

    private void initComponents() {

        k = 128;

        gcalendar = new JLabel();
        outlookcalendar = new JLabel();
        yahoocalendar = new JLabel();

        Image gcalendarimg = new ImageIcon(getClass().getClassLoader().getResource("resources/gcalendar.png")).getImage();
        Image outcalendarimg = new ImageIcon(getClass().getClassLoader().getResource("resources/outlookcalendar.png")).getImage();
        Image ycalendarimg = new ImageIcon(getClass().getClassLoader().getResource("resources/yahoo.png")).getImage();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Iniciar Sesión");
        setResizable(false);
        getContentPane().setLayout(new FlowLayout());

        ImageIcon icon = new ImageIcon(gcalendarimg.getScaledInstance(k, k, java.awt.Image.SCALE_SMOOTH));
        gcalendar.setIcon(new ImageIcon(gcalendarimg.getScaledInstance(k, k, java.awt.Image.SCALE_SMOOTH)));
        gcalendar.setText("Google Calendar");
        outlookcalendar.setIcon(new ImageIcon(outcalendarimg.getScaledInstance(k, k, java.awt.Image.SCALE_SMOOTH)));
        outlookcalendar.setText("Outlook Calendar");
        yahoocalendar.setIcon(new ImageIcon(ycalendarimg.getScaledInstance(k, k, java.awt.Image.SCALE_SMOOTH)));
        yahoocalendar.setText("Yahoo Calendar");

        gcalendar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                try {
                    cuentService.autenticar(userService.consultar().getNombre());
                    ventanaCalendario = new Calendario(userService, cuentService, calService, evService);
                    
                    dispose();
                } catch (Exception ex) {
                    Logger.getLogger(IniciarSesionServicio.class.getName()).log(Level.SEVERE, null, ex);
                }
               
            }
        });

        outlookcalendar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                JOptionPane.showMessageDialog(null, "Próximamente");
            }
        });

        yahoocalendar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                JOptionPane.showMessageDialog(null, "Próximamente");
            }
        });
        getContentPane().add(gcalendar);
        getContentPane().add(outlookcalendar);
        getContentPane().add(yahoocalendar);

        pack();
    }

}
