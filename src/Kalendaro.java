
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import servicios.CalendarioService;
import servicios.CuentaService;
import servicios.EventoService;
import servicios.UsuarioService;
import vista.IniciarSesionServicio;
import vista.Login;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Martín
 */
public class Kalendaro {

    /**
     * Inicio del programa, instanciando los services, y la interfaz de login
     * @param args 
     */
    public static void main(String[] args) {
        UsuarioService userService = new UsuarioService();
        CuentaService cuentService = new CuentaService();
        CalendarioService calService = new CalendarioService();
        EventoService evService = new EventoService();
        
        Login log = new Login(userService, cuentService, calService, evService);
    }

}
